<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| The order of routes must be like this, specially the 'goto' shoud be after 'shorturls'
|
*/

Auth::routes();

/** home route  */
Route::get('/', 'HomeController@index')->middleware('auth')->name('home');

/** shorturls routes  */
Route::resource('shorturls', 'ShorturlController');
Route::get('getdata', 'ShorturlController@getData')->name('shorturls.getdata');
Route::get('getlogdata', 'ShorturlController@getLogData')->name('shorturls.getlogdata');

/** redirect to original url  */
Route::get('/{key}', 'HomeController@goto')->name('goto');

/** switch languages route  */
Route::get('lang/{lang}', 'LanguageController@switchLang')->name('lang.switch');


