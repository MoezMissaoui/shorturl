<?php

namespace App\Http\Traits;

use App\Models\Shorturl;

use Illuminate\Support\Str;


trait ShorturlTrait {

    public function getUniqueKey()
    {
        $random_string = Str::random(6);
        while(Shorturl::where('key', $random_string )->exists()){
            $random_string = Str::random(6);
        }
        return $random_string;
    }
}