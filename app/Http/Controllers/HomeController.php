<?php

namespace App\Http\Controllers;

use App\Models\Shorturl;
use App\Models\Log;

use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Alert;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        return redirect('shorturls');
    }

    public function goto($key)
    {   
        $shorturl = Shorturl::where('key',$key)->first();
        $original_url = $shorturl->original_url ?? null ;
        if($original_url){
            Log::create([
                'shorturl_id'             =>  $shorturl->id ,
                'user_id'                 =>  Auth::user()->id ?? null ,
                'access_time'             =>  Carbon::now(),
                'id_adress'               =>  get_ip() ,
                'country'                 =>  ip_details()->country ?? '',
                'user_agent_navigator'    =>  http_user_agent(),
                'created_at'              =>  Carbon::now(),
            ]);
            return redirect($original_url);
        }
        Alert::error(trans('global.url_access_error'));
        abort_unless(false, 404);
    }

}
