<?php

namespace App\Http\Controllers;

use App\Models\Shorturl;
use App\Models\Log;

use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\StoreUrlRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Traits\ShorturlTrait;
use Illuminate\Http\Request;


class ShorturlController extends Controller
{
    use ShorturlTrait; 

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('shorturls.home');
    }


    public function getData()
    {
        $query = Shorturl::query()->where('create_by', Auth::user()->id)->orderBy('created_at', 'desc');
        $columns = DataTables::of($query);
        $columns
            ->editColumn('original_url', function (Shorturl $row) {
                return '<a href="'.($row->original_url ?? '#') .'" >'.  ($row->original_url ?? '#') . '</a>' ;

            })
            ->editColumn('key', function (Shorturl $row) {
                return '<a href="'. route('goto',$row->key ?? '') .'" >'.  short_url_full_form($row->key ?? '') . '</a>' ;
            })
            ->addColumn('actions', function (Shorturl $row) {
                $actions = '';
                    if(Auth::check() && $row->for_current_user()){
                        $actions .= '<a class="btn btn-xs btn-primary" href="' . route('shorturls.show', $row->id) . '" title="' . trans('global.view') . '">
                            <i class="fa fa-eye" aria-hidden="true"></i>
                        </a> ';

                        $actions .= '<form action="' . route('shorturls.destroy', $row->id) . '" method="POST" onsubmit="ConfirmDelete()" style="display: inline-block;">
                            <input name="_method" value="DELETE" type="hidden">
                            ' . csrf_field() . '
                            <button type="submit" class="btn btn-xs btn-danger" title="' . trans('global.delete') . '"><i class="fa fa-trash" aria-hidden="true"></i></button>
                        </form>';
                    }
                return $actions;
            })
            ->rawColumns(['original_url', 'key', 'actions'])
            ->escapeColumns(['actions']);
        return $columns->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('shorturls.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\StoreUrlRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUrlRequest $request)
    {
        if (Shorturl::count() >=20) {
            Shorturl::orderBy('created_at', 'asc')->limit(1)->delete();
        }
        $key = $this->getUniqueKey();
        $request->request->add(['key' => $key]); 
        $request->request->add(['create_by' => Auth::user()->id]); 
        $shorturl = Shorturl::create($request->all());
        if($shorturl)
            return redirect()->route('shorturls.index')->withToastSuccess(trans('global.url_create_success'));
        return redirect()->route('shorturls.index')->withToastError(trans('global.url_create_error'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Shorturl $shorturl
     * @return \Illuminate\Http\Response
     */
    public function show(Shorturl $shorturl)
    {
        return view('shorturls.show', compact('shorturl'));
    }

    public function getLogData(Request $request)
    {
    
        $query = Log::query()->where('shorturl_id', $request->get('shorturl_id') )->orderBy('access_time', 'desc');
        $columns = DataTables::of($query);
        $columns
            ->editColumn('user_id', function (Log $row) {
                return ($row->user->name ?? '') ;
            })
            ->editColumn('id_adress', function (Log $row) {
                return ($row->id_adress ?? '') ;
            })
            ->editColumn('country', function (Log $row) {
                return ($row->country ?? '') ;
            })
            ->editColumn('user_agent_navigator', function (Log $row) {
                return ($row->user_agent_navigator ?? '') ;
            })
            ->editColumn('access_time', function (Log $row) {
                return ($row->access_time ?? '') ;
            })
            ->rawColumns(['user_id', 'access_time', 'id_adress', 'country', 'user_agent_navigator'])
            ->escapeColumns(['actions']);
        return $columns->make(true);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Shorturl $shorturl
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shorturl $shorturl)
    {
        abort_unless($shorturl->for_current_user(), 403);
        $shorturl->delete();
        if($shorturl)
            return back()->withToastSuccess(trans('global.url_create_success'));
        return back()->withToastError(trans('global.url_delete_error'));

    }

}
