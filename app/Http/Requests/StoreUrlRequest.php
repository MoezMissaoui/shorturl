<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;


class StoreUrlRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'original_url' => ['required', 'url', 
                Rule::unique('shorturls','original_url')->whereNull('deleted_at')],
        ];
    }

    public function messages()
    {
        return [
            'original_url.required'        =>  trans('global.check_required', [ 'field' => trans('global.originalurl') ]), 
            'original_url.unique'          =>  trans('global.check_unique', [ 'field' => trans('global.originalurl') ]),        
            'original_url.url'          =>  trans('global.check_unique', [ 'field' => trans('global.originalurl') ]),        
        ];
    }
}
