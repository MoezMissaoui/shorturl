<?php

namespace App\Console\Commands;

use App\Models\Shorturl;

use Illuminate\Console\Command;

class CleanOldShorturls extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'oldshorturls:clean';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove links older than 24 hours';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $deleted_nbr = Shorturl::whereDate( 'created_at', '<=', now()->subDays(1))->delete();
        dd(trans('global.nett_db', ['nbr' => $deleted_nbr])) ;
    }
}
