<?php

namespace App\Models;

use App\Models\User;
use App\Models\Shorturl;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Log extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'shorturl_id',
        'user_id',
        'access_time',
        'id_adress',
        'country',
        'user_agent_navigator',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function shorturl()
    {
        return $this->belongsTo(Shorturl::class, 'shorturl_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
