<?php

namespace App\Models;

use App\Models\User;
use App\Models\Log;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Auth;

class Shorturl extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'original_url',
        'key',
        'create_by',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function create_by()
    {
        return $this->belongsTo(User::class, 'create_by', 'id');
    }

    public function logs()
    {
        return $this->hasMany(Log::class);
    }

    public function for_current_user()
    {
        return ($this->create_by == Auth::user()->id) ;
    }

}
