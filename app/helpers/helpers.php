<?php


// ------------------------------------------------------------------------

if(!function_exists('short_url_full_form')){
    function short_url_full_form($key){
        return url('').'/'.$key ;
    } 
}


// ------------------------------------------------------------------------

if(!function_exists('get_ip')){
    function get_ip() 
    {
        return $_SERVER['REMOTE_ADDR'];
    }
}

// ------------------------------------------------------------------------

if(!function_exists('ip_details')){
    function ip_details() 
    {
        $json = file_get_contents("http://ipinfo.io/{$_SERVER['REMOTE_ADDR']}");
        return json_decode($json);
    }
}


// ------------------------------------------------------------------------

if(!function_exists('http_user_agent')){
    function http_user_agent() 
    {
        return $_SERVER['HTTP_USER_AGENT'];
    }
}