@extends('errors::illustrated-layout')

@section('title', trans('global.404'))
@section('code', '404')
@section('message', $exception->getMessage() ?: trans('global.404'))