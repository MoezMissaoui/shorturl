@extends('layouts.app')

@section('styles')
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row" >
                        <div class="col-md-10" >
                            {{ trans('global.shorturllist') }}
                        </div>
                    </div>

                
                </div>
                <div class="card-body">
                    <form action="{{ route("shorturls.store") }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label" for="original_url">{{ trans('global.originalurl') }} <span class="text-danger">*</span></label>
                            <div class="col-sm-10">
                                <input type="text" id="original_url" name="original_url" class="form-control {{ $errors->has('original_url') ? 'border-danger' : '' }}"
                                 value="{{ old('original_url', isset($shorturl) ? $shorturl->original_url : '') }}">
                                @if($errors->has('original_url'))
                                    <span class="form-text m-b-none text-danger">
                                        {{ $errors->first('original_url') }}
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-4 col-sm-offset-2">
                                <a class="btn btn-secondary btn-sm" href="{{ route("shorturls.index") }}">{{ trans('global.cancel') }}</a>
                                <button class="btn btn-primary btn-sm" type="submit">{{ trans('global.save') }}</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
@endsection