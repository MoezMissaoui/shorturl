@extends('layouts.app')

@section('styles')
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
    <style>
        div.html5buttons {
            float: right !important;
        }
        div#DataTables_Table_0_wrapper{
            overflow-x: auto;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row" >
                        <div class="col-md-8" >
                            {{ trans('global.shorturllist') }}
                        </div>
                        @auth
                            @can('add_shorturl')
                                
                                <div class="col-md-4" >
                                    <a class="btn btn-success btn-sm float-right" href="{{ route("shorturls.create") }}">
                                        {{ trans('global.add') }} {{ trans('global.shorturl') }}
                                    </a>
                                </div>
                            @else
                                <div class="col-md-4 p-2 bg-danger text-white rounded" >
                                    {{ trans('global.max_lin_user') }}
                                </div>
                            @endcan
                        @endauth
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped shorturltable" >
                        <thead>
                          <tr>
                            <th scope="col">{{ trans('global.originalurl') }}</th>
                            <th scope="col">{{ trans('global.shorturl') }}</th>
                            <th scope="col">{{ trans('global.actions') }}</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <footer>
                            <tr>
                              <th scope="col">{{ trans('global.originalurl') }}</th>
                              <th scope="col">{{ trans('global.shorturl') }}</th>
                              <th scope="col">{{ trans('global.actions') }}</th>
                            </tr>
                          </footer>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('scripts')
    <script src="{{ asset('js/datatables.min.js') }}" ></script>

        <!-- Page-Level Scripts -->
        <script>
            $(document).ready(function(){
  
                $('.shorturltable').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    dom: '<"html5buttons"B>lTfgitp',
                    buttons: [
                        
                        {extend: 'excel', title: '{{ trans("global.shorturllist") }}'},
                        {extend: 'pdf', title: '{{ trans("global.shorturllist") }}'},
                        {
                            extend: 'print',
                            customize: function (win){
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');
                                $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                            }
                        }
                    ],
                    ajax: '{{ route("shorturls.getdata") }}',
                    order: [[ 0, 'asc' ]],
                    columns: [
                        { data: 'original_url', name: 'original_url', searchable: true, orderable: true },
                        { data: 'key', name: 'key', searchable: true, orderable: true },
                        { data: 'actions', name: 'actions', searchable: true, orderable: true },
                    ]

                });
            });
            function ConfirmDelete()
            {
                var x = confirm(@json(trans('global.areYouSure')));
                if (x)
                    return true;
                else
                    return false;
            }
        </script>

@endsection