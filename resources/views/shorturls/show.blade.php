@extends('layouts.app')

@section('styles')
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet">
    <style>
        div.html5buttons {
            float: right !important;
        }
        div#DataTables_Table_0_wrapper{
            overflow-x: auto;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row" >
                        <div class="col-md-8" >
                            {{ trans('global.logs') }} {{ trans('global.of') }} <a href="{{ route('goto', $shorturl->key )}}" target="_blank"> {{ $shorturl->key }} </a>
                        </div>

                        <div class="col-md-4" >
                            <a class="btn btn-secondary btn-sm float-right" href="{{ route("shorturls.index") }}">{{ trans('global.back') }}</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-striped logs" >
                        <thead>
                          <tr>
                            <th scope="col">{{ trans('global.user') }}</th>
                            <th scope="col">{{ trans('global.ip_adress') }}</th>
                            <th scope="col">{{ trans('global.country') }}</th>
                            <th scope="col">{{ trans('global.user_agent_navigator') }}</th>
                            <th scope="col">{{ trans('global.access_time') }}</th>
                          </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <footer>
                            <tr>
                              <th scope="col">{{ trans('global.user') }}</th>
                              <th scope="col">{{ trans('global.ip_adress') }}</th>
                              <th scope="col">{{ trans('global.country') }}</th>
                              <th scope="col">{{ trans('global.user_agent_navigator') }}</th>
                              <th scope="col">{{ trans('global.access_time') }}</th>
                            </tr>
                          </footer>
                      </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="{{ asset('js/datatables.min.js') }}" ></script>

        <!-- Page-Level Scripts -->
        <script>
            $(document).ready(function(){
  
                $('.logs').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    dom: '<"html5buttons"B>lTfgitp',
                    buttons: [
                        
                        {extend: 'excel', title: '{{ trans("global.shorturllist") }}'},
                        {extend: 'pdf', title: '{{ trans("global.shorturllist") }}'},
                        {
                            extend: 'print',
                            customize: function (win){
                                $(win.document.body).addClass('white-bg');
                                $(win.document.body).css('font-size', '10px');
                                $(win.document.body).find('table').addClass('compact').css('font-size', 'inherit');
                            }
                        }
                    ],
                    ajax: {
                        url: '{{ route("shorturls.getlogdata") }}',
                        data: function (d) {
                            d.shorturl_id = '{{$shorturl->id}}'
                        }
                    },
                    order: [[ 0, 'asc' ]],
                    columns: [
                        { data: 'user_id', name: 'user_id', searchable: true, orderable: true },
                        { data: 'id_adress', name: 'id_adress', searchable: true, orderable: true },
                        { data: 'country', name: 'country', searchable: true, orderable: true },
                        { data: 'user_agent_navigator', name: 'user_agent_navigator', searchable: true, orderable: true },
                        { data: 'access_time', name: 'access_time', searchable: true, orderable: true },
                    ]
                });
            });
        </script>

@endsection