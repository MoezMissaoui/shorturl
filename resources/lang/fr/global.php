<?php

return [

    'welcome'                => 'Bienvenu',
    'areYouSure'             => 'Êtes-vous sûr ?' ,
    'frensh'                 => 'Français',
    'english'                => 'Anglais',

    'shorturllist'           => 'Liste des URL courtes',

    'originalurl'            => 'Url Originale',
    'shorturl'               => 'Url courte',
    'actions'                => 'Actions',

    'of'                     => 'de',

    'add'                    => 'Ajouter',
    'delete'                 => 'Supprimer',
    'cancel'                 => 'Annuler',
    'save'                   => 'Enregister',
    'back'                   => 'Retour',

    'check_required'         => 'Le champ :field est obligatoire',
    'check_unique'           => 'l :field a déjà une URL courte',
    'check_url'              => 'The :field must be a valid URL',

    'url_create_success'     => 'URL courte créé avec succès!',
    'url_create_error'       => 'Erreur de création URL courte!',
    'url_delete_success'     => 'URL courte supprimé avec succès!',
    'url_delete_error'       => 'Erreur de suppression ShortURL!',
    'url_access_error'       => 'cette URL courte n\'existe pas!',

    'nett_db'                => ':nbr liens datant de plus que 24 heures sont supprimés.',


    'max_lin_user'           => 'Vous avez atteint le maximum de 5 liens.',

    'logs'                   => 'Logs',
    'user'                   => 'Utilisateur',
    'access_time'            => 'Le temps d\'accès',
    'ip_adress'              => 'Adresse IP',
    'country'                => 'Pays',
    'user_agent_navigator'   => 'Navigateur d\'agent utilisateur',

    '404'                    => 'Page non trouvée',
    'home'                   => 'Accueil',

];