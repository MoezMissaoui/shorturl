<?php

return [

    'welcome'                => 'Welcome',
    'areYouSure'             => 'Are You Sure ?' ,
    'frensh'                 => 'Frensh',
    'english'                => 'English',

    'shorturllist'           => 'List of short URls',

    'originalurl'            => 'Original Url',
    'shorturl'               => 'short Url',
    'actions'                => 'Actions',

    'of'                     => 'of',

    'add'                    => 'Add',
    'delete'                 => 'Delete',
    'cancel'                 => 'Cancel',
    'save'                   => 'Save',
    'save'                   => 'Save',
    'back'                   => 'Back',

    'check_required'         => 'The :field field is required',
    'check_unique'           => 'The :field already has a shorturl',
    'check_url'              => 'The :field must be a valid',

    'url_create_success'     => 'ShortURL Created Successfully!',
    'url_create_error'       => 'ShortURL creation error!',
    'url_delete_success'     => 'ShortURL Deleted Successfully!',
    'url_delete_error'       => 'ShortURL delete error!',
    'url_access_error'       => 'This ShortURL does not exist!',

    'nett_db'                => ':nbr Links older than 24 hours are removed.',

    'max_lin_user'           => 'You have reached the maximum of 5 links.',

    'logs'                   => 'Logs',
    'user'                   => 'User',
    'access_time'            => 'Access time',
    'ip_adress'              => 'IP Adress',
    'country'                => 'Country',
    'user_agent_navigator'   => 'User Agent Navigator',

    '404'                    => 'Page not found',
    'home'                   => 'Home',

];