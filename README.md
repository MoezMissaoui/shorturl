# ShortURL
## How to use

- Clone the repository with __git clone__
- Copy __.env.example__ file to __.env__ and edit database credentials there
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate__ (Some seeded data for testing)
- That's it: launch the main URL or go to __/register__ 